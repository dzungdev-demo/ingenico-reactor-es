package com.ingenico;

import javax.annotation.PostConstruct;

import org.reveno.atp.api.Reveno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.ingenico.backend.command.CreateAccountCmd;
import com.ingenico.backend.command.CreateAccountCmd.CreateAccountAction;
import com.ingenico.backend.command.CreateTransferCmd;
import com.ingenico.backend.command.CreateTransferCmd.CreateTransferAction;
import com.ingenico.backend.command.CreditAccountCmd;
import com.ingenico.backend.command.DebitAccountCmd;
import com.ingenico.backend.entity.Account;
import com.ingenico.backend.entity.Transfer;
import com.ingenico.backend.event.TransferChangedEvent;
import com.ingenico.backend.tx.action.CreateAccountTxAction;
import com.ingenico.backend.tx.action.CreateTransferTxAction;
import com.ingenico.backend.tx.action.CreditAccountTxAction;
import com.ingenico.backend.tx.action.DebitAccountTxAction;
import com.ingenico.backend.view.AccountView;
import com.ingenico.backend.view.TransferView;
import com.ingenico.util.Utils;

@Configuration
public class RevenoConfiguration {
	
	@Autowired
	private Reveno reveno;
	
	@PostConstruct
	public void onStartUp() {
		
		declareCommandHandler(reveno);
		declareTransactionActionHandler(reveno);
		
        reveno.domain().viewMapper(Account.class, AccountView.class, (id, e, ctx) -> {
        	return new AccountView(e.name, Utils.fromLong(e.balance));
        });
        reveno.domain().viewMapper(Transfer.class, TransferView.class, (id, e, ctx) -> {
        	
        	final AccountView fromViewAccountView = ctx.get(AccountView.class, e.fromAccountId);
        	final AccountView toViewAccountView = ctx.get(AccountView.class, e.toAccountId);
        	
        	return new TransferView(fromViewAccountView, toViewAccountView, 
        			Utils.fromLong(e.amount));
        	}
        );
        
        reveno.events().eventHandler(TransferChangedEvent.class, (e, m) -> {
            if (!m.isRestore()) {
                final AccountView accountView = reveno.query().find(AccountView.class, e.accountId);
            }
        });
        
        reveno.startup();
	}
	
//	@PreDestroy
//	public void onDestroy() {
//		reveno.shutdown();
//	}
	
	static void declareCommandHandler(final Reveno reveno) {
		reveno.domain().command(CreateAccountCmd.class, long.class, CreateAccountCmd::handleCommand);
		reveno.domain().command(CreateTransferCmd.class, long.class, CreateTransferCmd::handleCommand);
	}
	
	static void declareTransactionActionHandler(final Reveno reveno) {
		
		reveno.domain().transactionAction(CreateAccountAction.class, CreateAccountTxAction::handleTxAction);
		reveno.domain().transactionAction(CreditAccountCmd.class, CreditAccountTxAction::handleTxAction);
		reveno.domain().transactionAction(DebitAccountCmd.class, DebitAccountTxAction::handleTxAction);
		reveno.domain().transactionAction(CreateTransferAction.class, CreateTransferTxAction::handleTxAction);
		
	}
	
}

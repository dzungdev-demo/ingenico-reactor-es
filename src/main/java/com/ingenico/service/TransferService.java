package com.ingenico.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.reveno.atp.api.Reveno;
import org.reveno.atp.api.commands.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingenico.backend.command.CreateTransferCmd;

@Service
public class TransferService {
	
	@Autowired
	private Reveno reveno;
	
	public long createTransfer(final CreateTransferCmd createTransferCmd) {
		final CompletableFuture<Result<Long>> future = reveno.executeCommand(createTransferCmd);
		long transferId = -1;
		try {
			final Result<Long> result = future.get();
			transferId = result.getResult();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		} catch (final ExecutionException e) {
			e.printStackTrace();
		}
		return transferId;
	}
	
}

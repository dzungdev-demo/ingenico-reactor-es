package com.ingenico;



import java.net.URISyntaxException;
import java.util.Locale;

import org.reveno.atp.api.Configuration;
import org.reveno.atp.api.Reveno;
import org.reveno.atp.core.Engine;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class App {
	
	/**
	 * Locale resolver.
	 *
	 * @return the locale resolver
	 */
	@Bean
	public LocaleResolver localeResolver() {
		final SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(Locale.US);
		return slr;
	}

	/**
	 * Message source.
	 *
	 * @return the reloadable resource bundle message source
	 */
	@Bean
	public MessageSource messageSource() {
		final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:i18n/messages");
		messageSource.setCacheSeconds(1800);
		return messageSource;
	}

	/**
	 * Api.
	 *
	 * @return the docket for swagger
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build();
	}

	/**
	 * Cors configurer.
	 *
	 * @return the web mvc configurer
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(final CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*").allowedHeaders("Accept", "Content-Type", "Authorization")
						.allowedMethods("GET", "POST", "PUT", "DELETE").allowCredentials(false);
			}
		};
	}
	
	@Bean
	public Reveno reveno() {
		String path;
		try {
			path = App.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
			System.err.println("====> jar path: " + path);
			
			final Reveno reveno = new Engine(path + "ingenico-es");
			reveno.config().cpuConsumption(Configuration.CpuConsumption.HIGH);
			return reveno;
		} catch (final URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(final String[] args) throws URISyntaxException {
		
		final ConfigurableApplicationContext appCtx = SpringApplication.run(App.class , args);
		final Reveno reveno = appCtx.getBean(Reveno.class);
		
		
		
		
        
//        final long dzungAccId = reveno.executeSync(new CreateAccountCmd(DZUNG_ACC_NAME, 700_000));
//        final long hongAccId = reveno.executeSync(new CreateAccountCmd(HONG_ACC_NAME, 600_000));
//        
//        final int threadCount = 2000;
//        final int loopCount = 1;
//        final ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
//        final List<Future<Result<Void>>> futures = new ArrayList<>();
//        final CreateTransferCmd dzungToHongTransferCmd = new CreateTransferCmd(dzungAccId, hongAccId, 3);
//        final CreateTransferCmd hongToDzungTransferCmd = new CreateTransferCmd(hongAccId, dzungAccId, 1);
        
        
        
//        for (int i = 0; i < threadCount; ++i) {
//        	final Future<CompletableFuture> future;
//			final Callable<CompletableFuture> hongToDzungCallable = new Callable<CompletableFuture>() {
//				@Override
//				public CompletableFuture call() throws Exception {
//					for (int j = 0; j < loopCount; ++j) {
//						reveno.executeSync(hongToDzungTransferCmd);
//					}
//					return null;
//				}
//			};
//			future = executorService.submit(hongToDzungCallable);
//        }
        
//        for (int i = 0; i < 100_000; i++) {
//        	final Future<Result<Void>> future;
//			if (i % 2 == 0) {
//				future = reveno.executeCommand(dzungToHongTransferCmd);
//			} else {
//				future = reveno.executeCommand(hongToDzungTransferCmd);
//			}
//        	
//			futures.add(future);
//		}
//		futures.forEach(f -> {
//			try {
//				f.get();
//			} catch (InterruptedException | ExecutionException e) {
//				throw Exceptions.runtime(e);
//			}
//		});
//      
//        
//        final AccountView dzungAccountView = reveno.query().find(AccountView.class, dzungAccId);
//        System.err.println("==> Dzung balance: " + dzungAccountView.balance);
//        
//        final AccountView hongAccountView = reveno.query().find(AccountView.class, hongAccId);
//        System.err.println("==> Hong balance: " + hongAccountView.balance);
//        
//        reveno.shutdown();
		
	}
	
	
	
}
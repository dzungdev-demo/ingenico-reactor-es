package com.ingenico.backend.view;

public class TransferView {
	
	public final AccountView fromAccountView;
	public final AccountView toAccountView;
	public final double amount;
	
	public TransferView(final AccountView fromAccountView, final AccountView toAccountView, final double amount) {
		super();
		this.fromAccountView = fromAccountView;
		this.toAccountView = toAccountView;
		this.amount = amount;
	}
	
}

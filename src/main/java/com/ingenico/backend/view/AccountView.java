package com.ingenico.backend.view;

public class AccountView {
	
	public final String name;
    public final double balance;
//    public final Set<TransferView> transfers;
    
	public AccountView(final String name, final double balance) {
		super();
		this.name = name;
		this.balance = balance;
	}
    
//	public AccountView(final String name, final double balance, final Set<TransferView> transfers) {
//		super();
//		this.name = name;
//		this.balance = balance;
//		this.transfers = transfers;
//	}
    
    
    
}

package com.ingenico.backend.tx.action;

import org.reveno.atp.api.transaction.TransactionContext;

import com.ingenico.backend.command.CreditAccountCmd;
import com.ingenico.backend.entity.Account;
import com.ingenico.backend.event.TransferChangedEvent;
import com.ingenico.util.Utils;

public class CreditAccountTxAction {
	
	public static void handleTxAction(final CreditAccountCmd cmd, final TransactionContext ctx) {
		 ctx.repo().remap(cmd.accountId, Account.class, (id, e) -> e.addBalance(Utils.toLong(cmd.amount)));
		 ctx.eventBus().publishEvent(new TransferChangedEvent(cmd.accountId));
	}
	
}

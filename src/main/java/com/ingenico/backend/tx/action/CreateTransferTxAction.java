package com.ingenico.backend.tx.action;

import org.reveno.atp.api.transaction.TransactionContext;

import com.ingenico.backend.command.CreateTransferCmd.CreateTransferAction;
import com.ingenico.backend.entity.Transfer;
import com.ingenico.util.Utils;

public class CreateTransferTxAction {
	
	public static void handleTxAction(final CreateTransferAction action, final TransactionContext ctx) {
		ctx.repo().store(action.transferId, 
							new Transfer(action.transferId, 
										 action.info.fromAccountId, 
										 action.info.toAccountId, 
										 Utils.toLong(action.info.amount)));
	}
	
}

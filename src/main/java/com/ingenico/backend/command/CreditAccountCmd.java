package com.ingenico.backend.command;

public class CreditAccountCmd {
	
	public final long accountId;
    public final double amount;

    public CreditAccountCmd(final long accountId, final double amount) {
        this.accountId = accountId;
        this.amount = amount;
    }
   
}

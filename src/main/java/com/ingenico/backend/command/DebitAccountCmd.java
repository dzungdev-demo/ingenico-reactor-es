package com.ingenico.backend.command;

public class DebitAccountCmd {
	
	public final long accountId;
    public final double amount;

    public DebitAccountCmd(final long accountId, final double amount) {
        this.accountId = accountId;
        this.amount = amount;
    }
	
}

package com.ingenico.backend.entity;

import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;

public class Account {
	
	public final long id;
	public final String name;
    public final long balance;
    private final LongSet transfers;
    
	public Account(final long id, final String name, final long balance, 
			final LongSet transfers) {
		super();
		this.id = id;
		this.name = name;
		this.balance = balance;
		this.transfers = transfers;
	}
	
	public Account(final long id, final String name, final long balance) {
		super();
		this.id = id;
		this.name = name;
		this.balance = balance;
		this.transfers = new LongOpenHashSet();
	} 
	
	public LongCollection transfers() {
		return new LongOpenHashSet(transfers);
	}
	
	public Account addBalance(final long amount) {
		return new Account(id, name, balance + amount, transfers);
	}
	
	public Account reduceBalance(final long amount) {
		return new Account(id, name, balance - amount, transfers);
	}
    
}

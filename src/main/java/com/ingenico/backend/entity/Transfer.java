package com.ingenico.backend.entity;

public class Transfer {
	
	public final long id;
	public final long fromAccountId;
	public final long toAccountId;
	public final long amount;

	public Transfer(final long id, final long fromAccountId, final long toAccountId, final long amount) {
		super();
		this.id = id;
		this.fromAccountId = fromAccountId;
		this.toAccountId = toAccountId;
		this.amount = amount;
	}
	
}

package com.ingenico.api.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.reveno.atp.api.Reveno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ingenico.App;
import com.ingenico.api.rest.dto.CreateTransferDTO;
import com.ingenico.api.rest.util.TestUtil;
import com.ingenico.backend.command.CreateAccountCmd;
import com.ingenico.backend.view.AccountView;
import com.ingenico.service.TransferService;

import reactor.bus.EventBus;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@IntegrationTest({ "server.port=0", "management.port=0" })
public class TransferApiIntTest {

	@Autowired
	private Reveno reveno;

	@Autowired
	private EventBus eventBus;
	
	/** The transfer service. */
	@Autowired
	private TransferService transferService;

	/** The rest transfer api mock mvc. */
	private MockMvc restTransferApiMockMvc;

	private static final String DZUNG_ACC_NAME = "Dzung";
	private static final String HONG_ACC_NAME = "Hong";
	String TRANSFERS_API_BASE_PATH = "/api/v1/transfers";

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		final TransferApi transferApi = new TransferApi();
		ReflectionTestUtils.setField(transferApi, "transferService", transferService);
//		ReflectionTestUtils.setField(transferApi, "eventBus", eventBus);

		restTransferApiMockMvc = MockMvcBuilders.standaloneSetup(transferApi).build();
		if (!reveno.isStarted()) reveno.startup();
	}
	
	@After
	public void shutdownReveno() {
//		reveno.shutdown();
	}

	@Test
	public void testGivenDzung30_000andHong20_000_whenDzungTransferHong3AndHongTransferDzung1_5000timesForEach_thenDzung20_000AndHongIs10_000() {

		final long dzungAccId = reveno.executeSync(new CreateAccountCmd(DZUNG_ACC_NAME, 30_000));
		final long hongAccId = reveno.executeSync(new CreateAccountCmd(HONG_ACC_NAME, 20_000));
		final int threadCount = 100;
		final int loopCount = 1;
		final ExecutorService executorService = Executors.newFixedThreadPool(100);
		// final List<Future<Result<Void>>> futures = new ArrayList<>();
		final CreateTransferDTO dzungToHongTransferDTO = new CreateTransferDTO(dzungAccId, hongAccId, 3);
		final CreateTransferDTO hongToDzungTransferDTO = new CreateTransferDTO(hongAccId, dzungAccId, 1);

		final List<Future> futures = new ArrayList<Future>();
		for (int i = 0; i < 10000; i++) {
//			for (int j = 0; j < threadCount; j++) {
				final CreateTransferDTO transferDTO;
				if (i % 2 == 0) {
					transferDTO = dzungToHongTransferDTO;
				} else {
					transferDTO = hongToDzungTransferDTO;
				}
				final Future<CompletableFuture> future;
				final Callable<CompletableFuture> callable = new Callable<CompletableFuture>() {
					@Override
					public CompletableFuture call() throws Exception {
						restTransferApiMockMvc
								.perform(post(TRANSFERS_API_BASE_PATH)
										.content(TestUtil.convertObjectToJsonBytes(transferDTO))
										.contentType(MediaType.APPLICATION_JSON_UTF8))
								.andExpect(status().isOk());
						return null;
					}
				};
				future = executorService.submit(callable);
				futures.add(future);
//			}
		}
		futures.forEach(f -> {
			try {
				f.get();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});

		final AccountView dzungAccountView = reveno.query().find(AccountView.class, dzungAccId);
		final AccountView hongAccountView = reveno.query().find(AccountView.class, hongAccId);
		
		
		
		Assertions.assertThat(dzungAccountView.balance).isEqualTo(20_000);
		Assertions.assertThat(hongAccountView.balance).isEqualTo(30_000);
		
	}

}
